<?php

/**
 * @param $uid
 * @return
 * This function lists the available revisions
 */
function qm_registration_user_revisions($uid){

  global $user;

  //Change the title by drupal built in function.
  drupal_set_title('User Account Revisions');


  $account_revisions_log = db_select('qm_registration_user_revision', 'ur')
    ->fields('ur', array('uid', 'account_name', 'revision_id', 'euid', 'editor_name', 'changed'))
    ->condition('uid', $uid)
    ->orderBy('revision_id','DESC')
    ->execute()->fetchAll();

  // find the current revision id so that we can differentiate it from others
  $current_revision_id = db_select('qm_registration_user_revision_assign', 'ura')
    ->fields('ura', array('revision_id' ))
    ->condition('uid', $uid)
    ->execute()->fetchObject();


  if(!empty($account_revisions_log)) {
    foreach($account_revisions_log as $rev_log){

      $rev_id[] = $rev_log->revision_id;

      //if current then no delete or revert link
      if($rev_log->revision_id == $current_revision_id->revision_id) {
        $items[] = array(
          'name' => l(date('j M Y - h:ia', $rev_log->changed),'user/'.$rev_log->uid.'/revisions/'.$rev_log->revision_id.'/view').' by ' . l($rev_log->editor_name, 'user/'. $rev_log->euid),
          'revert' => '<i>current version</i>',
          'delete' => '',
        );

      }else{
        $items[] = array(
          'name' => l(date('j M Y - h:ia', $rev_log->changed),'user/'.$rev_log->uid.'/revisions/'.$rev_log->revision_id.'/view').' by '.l($rev_log->editor_name, 'user/'. $rev_log->euid),
          'revert' => l('revert','user/'.$rev_log->uid.'/revisions/'.$rev_log->revision_id.'/revert'),
          'delete' => l('delete','user/'.$rev_log->uid.'/revisions/'.$rev_log->revision_id.'/delete'),
        );
      }

    }

    $header = array('REVISION', 'OPERATIONS', '' );
    $output = theme('table', array(
                                  'header' => $header,
                                  'rows' => $items,
                             ));
  }else{

    $items[] = array(
      'name' => "There are no revision here.",
    );

    $header = array('REVISION');
    $output = theme('table', array(
                                  'header' => $header,
                                  'rows' => $items,
                             )
    );
  }


  return $output;

}





/**
 * @param $uid
 * @param $revision_id
 * @return
 * For showing a certain profile state
 */

function qm_registration_user_revision_detail($uid, $revision_id){

  $account_revisions_log = db_select('qm_registration_user_revision', 'ur')
    ->fields('ur', array('account_name', 'mail','status', 'timezone', 'language', 'picture', 'registration_no', 'ref_name', 'ref_relation', 'ref_registration_no', 'ref_contact', 'reg_status', 'changed'))
    ->condition('revision_id', $revision_id)
    ->condition('uid', $uid)
    ->execute()->fetchObject();



  //Set title by drupal default drupal_set_title() function.
  drupal_set_title('Profile State:');

  // prepare the user information to be shown in a table
  $items = array();

  $items['account_name'] = array(
    'index' => '<b>Name:</b>',
    'account_name' => $account_revisions_log->account_name
  );

  $image_file_name = db_select('file_managed', 'fm')
    ->fields('fm', array('filename'))
    ->condition('fid', $account_revisions_log->picture)
    ->execute()->fetchObject();

  $default_img = '/sites/default/files/pictures/default-user.png';
  if(!empty($image_file_name->filename)) {
    $usr_img = '/sites/default/files/styles/thumbnail/public/pictures/user-picture/' . $image_file_name->filename ;
  }else{
    $usr_img = 'none';
  }

  if($usr_img == 'none') {
    $img_src = $default_img;
  }else{
    $img_src = $usr_img;
  }

  $items['picture'] = array(
    'index' => '<b>Profile Picture:</b>',
    'picture' => '<img src="' . $img_src . '" alt="profile picture" />',
  );

  // As different fields are there in the user form so find the values like- first name, last name, contact etc.
  // first find the related field names
  $field_name_query = db_select('field_config_instance', 'fci')
    ->fields('fci', array('field_id','field_name','entity_type','bundle','deleted'))
    ->condition('entity_type', 'user')
    ->condition('bundle', 'user')
    ->condition('deleted', 0)
    ->execute()->fetchAll();

 // now from the field tables find the values. and add them to the $items variable
  foreach($field_name_query as $key) {

    $index = $key->field_name;
    $data_table = 'field_revision_' . $index;
    $field_data = db_select($data_table, 'dt')
      ->fields('dt', array($index .'_value'))
      ->condition('revision_id', $revision_id)
      ->condition('entity_id', $uid)
      ->execute()->fetchObject();

    if(!empty($field_data)) {
      $field_value = $index .'_value';

      $items[$field_data->$field_value] = array(
        'index' => '<b>' . $index. ':</b>',
        $field_data->$field_value => $field_data->$field_value,
      );
    }
  }

  $items['mail'] = array(
    'index' => '<b>E-mail Address:</b>',
    'mail' => $account_revisions_log->mail
  );
  $items['registration_no'] = array(
    'index' => '<b>Registration Number:</b>',
    'registration_no' => $account_revisions_log->registration_no
  );
  $items['ref_name'] = array(
    'index' => '<b>Reference Person:</b>',
    'ref_name' => $account_revisions_log->ref_name
  );
  $items['ref_relation'] = array(
    'index' => '<b>Reference Relation:</b>',
    'ref_relation' => $account_revisions_log->ref_relation
  );
  $items['ref_registration_no'] = array(
    'index' => '<b>Reference Registration no:</b>',
    'ref_registration_no' => $account_revisions_log->ref_registration_no
  );
  $items['ref_contact'] = array(
    'index' => '<b>Reference Contact:</b>',
    'ref_contact' => $account_revisions_log->ref_contact
  );
  $items['reg_status'] = array(
    'index' => '<b>User status:</b>',
    'reg_status' => $account_revisions_log->reg_status
  );
  $items['changed'] = array(
    'index' => '<b>Modification Time:</b>',
    'changed' => date('j M Y - h:ia', $account_revisions_log->changed)
  );


  $output = theme('table', array(
                                'header' => array('Index', 'Value'),
                                'rows' => $items,
                           )
  );


  return $output;
}



/**
 * @param $uid
 * @param $revision_id
 * @return void
 * This functions reverts present profile state to another state
 * For reverting the state following tables must be updated {qm_registration_user_revision_assign}, {users},
 * the field_data_  & field_revision_ type tables related to user
 */
function qm_registration_user_revision_revert($uid, $revision_id) {


  $user_revisions_assign = db_update('qm_registration_user_revision_assign')
    ->fields(array(
                  'revision_id' => $revision_id,
             ))
    ->condition('uid', $uid)
    ->execute();

  //Find data from {qm_registration_user_revision} table for updating the users table.
  $user_revision = db_select('qm_registration_user_revision', 'ur')
    ->fields('ur', array('uid','account_name','revision_id','mail','theme','signature', 'signature_format', 'status', 'timezone', 'language', 'picture'))
    ->condition('revision_id', $revision_id)
    ->execute()->fetchObject();

  $comment = db_update('users')
    ->fields(array(
                  'name' => $user_revision->account_name,
                  'mail' => $user_revision->mail,
                  'theme' => $user_revision->theme,
                  'signature' => $user_revision->signature,
                  'signature_format' => $user_revision->signature_format,
                  'status' => $user_revision->status,
                  'timezone' => $user_revision->timezone,
                  'language' => $user_revision->language,
                  'picture' => $user_revision->picture,
             ))
    ->condition('uid',$uid)
    ->execute();


  // finally update the field_data_field_ tables according to field_revision_field_ tables
  // for that first find the user related field tables.
  $field_name_query = db_select('field_config_instance', 'fci')
    ->fields('fci', array('field_id','field_name','entity_type','bundle','deleted'))
    ->condition('entity_type', 'user')
    ->condition('bundle', 'user')
    ->condition('deleted', 0)
    ->execute()->fetchAll();


  foreach($field_name_query as $key) {

    $index = $key->field_name;
    $revision_table_name = 'field_revision_' . $index;

    $field_rev = db_select($revision_table_name, 'rt')
      ->fields('rt', array('entity_id','revision_id', $index .'_value'))
      ->condition('revision_id', $revision_id)
      ->condition('entity_id', $uid)
      ->execute()->fetchObject();

    $index_value = $index .'_value';

    if(!empty($field_rev)) {
      $data_table_name = 'field_data_' . $index;
      $data_table_update = db_update($data_table_name)
        ->fields(array(
                      'revision_id' => $field_rev->revision_id,
                      $index .'_value' => $field_rev->$index_value,
                 ))
        ->condition('entity_id', $uid)
        ->execute();
    }

  }


  //When run revert function then clear cache automatically.
  //  $core = array('cache', 'cache_path', 'cache_filter', 'cache_bootstrap', 'cache_page');
  $core = array('cache');
  $cache_tables = array_merge(module_invoke_all('flush_caches'), $core);
  foreach ($cache_tables as $table) {
    cache_clear_all('*', $table, TRUE);
  }

  $path = "user/".$uid."/revisions";
  drupal_goto($path);

}



/**
 * @param $uid
 * @param $revision_id
 * @return void
 * for deleting a revision data has to be deleted from the {qm_registration_user_revision} table and all the
 * field tables
 */
function qm_registration_user_revision_delete($uid, $revision_id) {

  //When click delete from revision log then deleted a row from comment_revision according to selected revision_id.
  $user_rev_del = db_delete('qm_registration_user_revision')
    ->condition('revision_id', $revision_id)
    ->execute();

  $field_name_query = db_select('field_config_instance', 'fci')
    ->fields('fci', array('field_id','field_name','entity_type','bundle','deleted'))
    ->condition('entity_type', 'user')
    ->condition('bundle', 'user')
    ->condition('deleted', 0)
    ->execute()->fetchAll();


  foreach($field_name_query as $key) {
    $index = $key->field_name;
    $revision_table_name = 'field_revision_' . $index;
    $body_rev = db_delete($revision_table_name)
      ->condition('revision_id', $revision_id)
      ->condition('entity_id', $uid)
      ->execute();

  }

  $path = "user/" . $uid . "/revisions";
  drupal_goto($path);

}