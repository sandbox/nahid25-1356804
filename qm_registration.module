<?php


/**
 * @return array
 * implements hook_menu()
 */
function qm_registration_menu(){

  $items = array();

  $items['admin/config/terms'] = array(
    'title' => ' Registration Terms & Conditions setting form',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('qm_registration_form'),
    'access callback' => TRUE,
    'file' => 'term_form.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  //this page is user one time initially for swaping data from users table to qm_registration_user table
  $items['registration-swap'] = array(
    'title' => 'Registration Table Swap',
    'page callback' => 'qm_registration_table_swap',
//      'access arguments' =>  array('access administration pages'),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['user/%/revisions'] = array(
    'title' => 'Revisions',
    'page callback' => array('qm_registration_user_revisions'),
    'page arguments' => array(1),
    'access arguments' =>  array('access administration pages'),
    'file' => 'qm_registration.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['user/%/revisions/%/view'] = array(
    'title' => 'User Revisions Details',
    'page callback' => array('qm_registration_user_revision_detail'),
    'page arguments' => array(1,3),
    'access arguments' =>  array('access administration pages'),
    'file' => 'qm_registration.admin.inc',
    'type' => MENU_CALLBACK
  );
  $items['user/%/revisions/%/revert'] = array(
    'title' => 'User Revisions Revert',
    'page callback' => array('qm_registration_user_revision_revert'),
    'page arguments' => array(1,3),
    'access arguments' =>  array('access administration pages'),
    'file' => 'qm_registration.admin.inc',
    'type' => MENU_CALLBACK
  );
  $items['user/%/revisions/%/delete'] = array(
    'title' => 'User Revisions Delete',
    'page callback' => array('qm_registration_user_revision_delete'),
    'page arguments' => array(1,3),
    'access arguments' =>  array('access administration pages'),
    'file' => 'qm_registration.admin.inc',
    'type' => MENU_CALLBACK
  );

  return $items;
}


/**
 * @param $form
 * @param $form_state
 * @return void
 * implements hook_form_FORM_ID_alter()
 * Modifies the user registration form
 */

function qm_registration_form_user_register_form_alter(&$form, &$form_state) {

  $default_terms = "I give my consent to abide by the terms & conditions
                      of services of this website and if I break any terms then Quantum Foundation
                      has the sole authority of taking necessary legal actions.";

  // add a new validate function to the user form to handle the legal agreement
  $form['#validate'][] = 'qm_registration_user_form_validate';
  $form['#submit'][] = 'qm_registration_user_register_form_submit';

  // add a field set to wrap the legal agreement
  $form['account']['qm_registration_terms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Terms & Conditions'),

  );

  $form['account']['qm_registration_terms']['terms'] = array(
    '#type' => 'fieldset',
    '#description' => variable_get('terms', $default_terms),
  );
  // add the legal agreement radio buttons
  $form['account']['qm_registration_terms']['decision'] = array(
    '#type' => 'radios',
    '#description' => t('By registering at %site-name, you agree that you have read and understood all the terms and conditions
			    of use and give your consent to abide by them.',
                        array('%site-name' => variable_get('site_name', 'drupal'))),
    '#default_value' => 0,
    '#options' => array(t('I disagree'), t('I agree')),
  );

  $default_option_status = 'a';
  $default_registration_no = NULL;
  $default_associate_ref_name = NULL;
  $default_associate_ref_id = NULL;
  $default_associate_ref_contact = NULL;
  $default_associate_ref_relation = NULL;

  $option_status = array('p'=>'Pro-Master', 'g'=>'Graduate', 'a' => 'Associate');

  $form['user_reg_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Quantum Registration Information'),

  );

  $form['user_reg_fieldset']['user_status'] = array(
    '#title' => t('User Status:'),
    '#description' => t('If you are not a Quantum Member please select "Associate" <br /> And provide the following information of a Quantum Member known to you.'),
    '#type' => 'radios',
    '#default_value' => $default_option_status,
    '#options' => $option_status
  );
  $form['user_reg_fieldset']['promaster_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Pro-Master registration No'),
    '#size'=> 30,
    '#default_value' => $default_registration_no,
    '#description' => t('Example: M-1500/17'),
    '#states' => array(
      'visible' => array(
        ':input[name="user_status"]' => array('value' => 'p')
      ),
    ),
  );
  $form['user_reg_fieldset']['graduate_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Graduate registration No'),
    '#size'=> 30,
    '#default_value' => $default_registration_no,
    '#description' => t('Example: 999/345 or F-888/342 etc.'),
    '#states' => array(
      'visible' => array(
        ':input[name="user_status"]' => array('value' => 'g')
      ),
    ),
  );

  $form['user_reg_fieldset']['associate_ref_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference Name'),
    '#size'=> 60,
    '#default_value' => $default_associate_ref_name,
    '#description' => t('Name of your reference person.'),
    '#states' => array(
      'visible' => array(
        ':input[name="user_status"]' => array('value' => 'a')
      ),
    ),
  );
  $form['user_reg_fieldset']['associate_ref_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference registration No.'),
    '#size'=> 30,
    '#default_value' => $default_associate_ref_id,
    '#description' => t('Quantum Registration ID of your reference person. Correct Format: 1200/227 or F-1644/300 or M-1200/17'),
    '#states' => array(
      'visible' => array(
        ':input[name="user_status"]' => array('value' => 'a')
      ),
    ),
  );
  $form['user_reg_fieldset']['associate_ref_contact'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference phone No.'),
    '#size'=> 30,
    '#default_value' => $default_associate_ref_contact,
    '#description' => t('Phone no. of your reference person.'),
    '#states' => array(
      'visible' => array(
        ':input[name="user_status"]' => array('value' => 'a')
      ),
    ),
  );
  $form['user_reg_fieldset']['associate_ref_relation'] = array(
    '#type' => 'textfield',
    '#title' => t('Relation with Reference Person'),
    '#size'=> 30,
    '#default_value' => $default_associate_ref_relation,
    '#description' => t('brother, colleague, friend etc.'),
    '#states' => array(
      'visible' => array(
        ':input[name="user_status"]' => array('value' => 'a')
      ),
    ),
  );

}


/**
 * @param $form
 * @param $form_state
 * @return void
 * Validation function for registration form.
 */


function qm_registration_user_form_validate($form, &$form_state) {

  // Did user agree on the registration form? Not applicable for profile form.
  if ($form_state['input']['decision'] <> 1) {
    form_set_error('decision', t('You must agree to the Terms & Conditions before registration
                                      can be completed.'));    }

  // 'p', 'g' or 'a' user status:
  $user_status = $form_state['values']['user_status'];

  // for associates:  No validation

  //for pro-masters:
  if ($user_status == 'p') {
    if(empty($form_state['values']['promaster_id'])){
      form_set_error('promaster_id', 'Please enter your promaster ID no.');
    }

    // check if promaster Id is in correct format:
    if(!empty($form_state['values']['promaster_id'])) {
      $ptn_master = "/^M-[0-9]+\/[0-9]+$/";
      $str = trim($form_state['values']['promaster_id']);
      preg_match($ptn_master, $str, $matches_m);

      if (empty($matches_m[0])) {
        form_set_error('promaster_id', 'Please correct your promaster ID no.');
      }
    }
  }
  //for graduates:
  if ($user_status == 'g') {
    if(empty($form_state['values']['graduate_id'])){
      form_set_error('graduate_id', 'Please enter your Quantum Graduate ID no.');
    }
    // check if graduate Id is in correct format:
    if(!empty($form_state['values']['graduate_id'])) {
      $ptn_graduate = "/^F-[0-9]+\/[0-9]+$|^[0-9]+\/[0-9]+$/";
      $str = trim($form_state['values']['graduate_id']);
      preg_match($ptn_graduate, $str, $matches_g);

      if (empty($matches_g[0])){
        form_set_error('graduate_id', 'Please correct your Quantum Graduate ID no.');
      }
    }
  }

  //for associates:
  if ($user_status == 'a' && !empty($form_state['values']['associate_ref_name'])) {

    if (empty($form_state['values']['associate_ref_id'])) {
      form_set_error('associate_ref_id', 'Please enter Reference registration no.');
    }

    if (empty($form_state['values']['associate_ref_contact'])) {
      form_set_error('associate_ref_contact', 'Please enter Reference contact no.');
    }

  }


}



/**
 * @param $form
 * @param $form_state
 * @return void
 * submit function for the registration form.
 * The validation function for this form is below: same as profile form validation function.
 */
function qm_registration_user_register_form_submit($form, &$form_state) {

  if ($form_state['values']['user_status'] == 'p') {
    $reg_no = $form_state['values']['promaster_id'];
  }elseif ($form_state['values']['user_status'] == 'g') {
    $reg_no = $form_state['values']['graduate_id'];
  }elseif ($form_state['values']['user_status'] == 'a') {

    //  Find the highest uid no for quantum associates
    $sub_query= db_select('qm_registration_user');
    $sub_query->condition('registration_no', 'QA%', "LIKE");
    $sub_query->addExpression("max(uid)", 'max_uid');

    $max_id = $sub_query->execute()->fetchObject();

    $result = db_select('qm_registration_user', 'reg')->fields('reg', array('uid', 'registration_no'));
    $result->condition('uid', $max_id->max_uid, '=');

    $record = $result->execute()->fetchObject()->registration_no;


    //$record = $result->fetchObject()->registration_no;
    // $record returns the last associate registration ID. example- QA-15/1203
    //Now we need to find the ID only. on the above example find the no. 15 excluding 'QA-' & '/1203'

    $date = date('ym', REQUEST_TIME);
    $id = substr($record, strpos($record, '-')+1, strpos(substr($record, strpos($record, '-')+1), '/'));

    // As the last part /1203 remains constant for a month so if it matches the $date then we are still
    // in the same month. If it is greater, then we are on the first date of the next month.

    if ($date == substr($record, -4))  {
      $reg_no = 'QA-' .  ($id + 1) . '/' . $date;
    }elseif ($date > substr($record, -4)) {
      $reg_no = 'QA-1/' . $date;
    }else {

      drupal_set_message(t('Please contact Admin. Sending the following message:<b> Error generating QA reg.</b>'), 'error');
      drupal_goto('user');

      //@todo send error notification mail to admin.
    }
  }

  $qm_user = db_insert('qm_registration_user')
    ->fields(array(
                  'uid' => $form_state['values']['uid'],
                  'registration_no' => $reg_no,
                  'ref_name' => $form_state['values']['associate_ref_name'],
                  'ref_registration_no' => $form_state['values']['associate_ref_id'],
                  'ref_contact' => $form_state['values']['associate_ref_contact'],
                  'ref_relation' => $form_state['values']['associate_ref_relation'],
                  'status' => $form_state['values']['user_status']
             ))
    ->execute();

}


/**
 * @param $form
 * @param $form_state
 * @return void
 * implements hook_form_FORM_ID_alter()
 * Modifies the profile form
 */


function qm_registration_form_user_profile_form_alter(&$form, &$form_state) {

  // add a  validation function to the profile form
  $form['#validate'][] = 'qm_registration_user_profile_form_validate';
  //add submit function for the profile form
  $form['#submit'][] = 'qm_registration_user_profile_form_submit';

  $uid = $form_state['build_info']['args'][0]->uid;

  //for profile form: setting default values
  $query_user = db_select('qm_registration_user', 'ru')
    ->fields('ru', array('uid', 'registration_no', 'ref_name', 'ref_registration_no', 'ref_contact', 'status', 'ref_relation'))
    ->condition('uid', $uid)
    ->execute();

  $user_data = $query_user->fetchObject();

  // for setting default values in profile form.
  if (isset($user_data->registration_no)) {
    $default_registration_no = $user_data->registration_no;
  }else{
    // in case data is missing for registration number in profile form.
    $default_registration_no = '';
  }

  if(isset($user_data->status)) {
    $default_option_status = $user_data->status; // for example: associate, graduate, pro-master etc.
  }else{
    $default_option_status = 'p';
  }
  // other information for associates:
  if (isset($user_data->ref_name)) {
    $default_associate_ref_name = $user_data->ref_name;
  }else{
    // in case data is missing for reference name in profile form (existing associates).
    $default_associate_ref_name = '';
  }
  if (isset($user_data->ref_registration_no)) {
    $default_associate_ref_id = $user_data->ref_registration_no;
  }else{
    // in case data is missing for reference ID in profile form (existing associates).
    $default_associate_ref_id = '';
  }
  if (isset($user_data->ref_contact)) {
    $default_associate_ref_contact = $user_data->ref_contact;
  }else{
    // in case data is missing for reference contact in profile form (existing associates).
    $default_associate_ref_contact = '';
  }
  if (isset($user_data->ref_relation)) {
    $default_associate_ref_relation = $user_data->ref_relation;
  }else{
    // in case data is missing for reference contact in profile form (existing associates).
    $default_associate_ref_relation = '';
  }


  $option_status = array('p'=>'Pro-Master', 'g'=>'Graduate', 'a' => 'Associate');

  $form['user_prof_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Quantum Registration Information'),

  );

  if (user_access('access administration pages')) {

    $form['user_prof_fieldset']['user_status'] = array(
      '#title' => t('User Status:'),
      '#description' => t('If you are not a Quantum Member please select "Associate" <br /> And provide the following information of a Quantum Member known to you.'),
      '#type' => 'radios',
      '#default_value' => $default_option_status,
      '#options' => $option_status
    );
    $form['user_prof_fieldset']['promaster_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Pro-Master registration No'),
      '#size'=> 30,
      '#default_value' => $default_registration_no,
      '#description' => t('Example: M-1500/17'),
      '#states' => array(
        'visible' => array(
          ':input[name="user_status"]' => array('value' => 'p')
        ),
      ),
    );
    $form['user_prof_fieldset']['graduate_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Graduate registration No'),
      '#size'=> 30,
      '#default_value' => $default_registration_no,
      '#description' => t('Example: 999/345 or F-888/342 etc.'),
      '#states' => array(
        'visible' => array(
          ':input[name="user_status"]' => array('value' => 'g')
        ),
      ),
    );

    $form['user_prof_fieldset']['associate_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Associate registration No'),
      '#size'=> 30,
      '#default_value' => $default_registration_no,
      '#description' => t('Example: QA-147/1203 etc.'),
      '#states' => array(
        'visible' => array(
          ':input[name="user_status"]' => array('value' => 'a')
        ),
      ),
    );
  }


  if(!empty($default_option_status)) {
    if($default_option_status == 'a'){
      $form['associate_option'] = array('#type' => 'hidden', '#value' => 'a');

      $form['user_prof_fieldset']['associate_ref_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Reference Name'),
        '#size'=> 60,
        '#default_value' => $default_associate_ref_name,
        '#description' => t('Name of your reference person.'),
        '#states' => array(
          'visible' => array(
            ':input[name="user_status"]' => array('value' => 'a')
          ),
        ),
      );
      $form['user_prof_fieldset']['associate_ref_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Reference registration No.'),
        '#size'=> 30,
        '#default_value' => $default_associate_ref_id,
        '#description' => t('Quantum Registration ID of your reference person. Correct Format: 1200/227 or F-1644/300 or M-1200/17'),
        '#states' => array(
          'visible' => array(
            ':input[name="user_status"]' => array('value' => 'a')
          ),
        ),
      );
      $form['user_prof_fieldset']['associate_ref_contact'] = array(
        '#type' => 'textfield',
        '#title' => t('Reference phone No.'),
        '#size'=> 30,
        '#default_value' => $default_associate_ref_contact,
        '#description' => t('Phone no. of your reference person.'),
        '#states' => array(
          'visible' => array(
            ':input[name="user_status"]' => array('value' => 'a')
          ),
        ),
      );
      $form['user_prof_fieldset']['associate_ref_relation'] = array(
        '#type' => 'textfield',
        '#title' => t('Relation with Reference Person'),
        '#size'=> 30,
        '#default_value' => $default_associate_ref_relation,
        '#description' => t('brother, colleague, friend etc.'),
        '#states' => array(
          'visible' => array(
            ':input[name="user_status"]' => array('value' => 'a')
          ),
        ),
      );

    }
  }
}




function qm_registration_user_profile_form_validate($form, &$form_state) {

  //for associates:
  if (!empty($form_state['values']['associate_ref_name'])) {

    if (empty($form_state['values']['associate_ref_id'])) {
      form_set_error('associate_ref_id', 'Please enter Reference registration no.');
    }

    if (empty($form_state['values']['associate_ref_contact'])) {
      form_set_error('associate_ref_contact', 'Please enter Reference contact no.');
    }

  }


}





/**
 * @param $form
 * @param $form_state
 * @return void
 * submit function for user profile form.
 */
function qm_registration_user_profile_form_submit($form, &$form_state) {

  if(!empty($form_state['values']['user_status'])){
    if ($form_state['values']['user_status'] == 'p') {
      $reg_no = $form_state['values']['promaster_id'];
    }elseif ($form_state['values']['user_status'] == 'g') {
      $reg_no = $form_state['values']['graduate_id'];
    }elseif ($form_state['values']['user_status'] == 'a') {
      $reg_no = $form_state['values']['associate_id'];
    }
  }
  if (user_access('access administration pages')) {

    $uid = $form_state['build_info']['args'][0]->uid;
    $edited = db_update('qm_registration_user')
      ->fields(array(
                    'registration_no' => $reg_no,
                    'status' => $form_state['values']['user_status']
               ))
      ->condition('uid', $uid)
      ->execute();

  }
  if (!empty($form_state['values']['associate_option'])) {
    $uid = $form_state['build_info']['args'][0]->uid;
    $edited = db_update('qm_registration_user')
      ->fields(array(
                    'ref_name' => $form_state['values']['associate_ref_name'],
                    'ref_registration_no' => $form_state['values']['associate_ref_id'],
                    'ref_contact' => $form_state['values']['associate_ref_contact'],
                    'ref_relation' => $form_state['values']['associate_ref_relation'],
               ))
      ->condition('uid', $uid)
      ->execute();
  }
}




/**
 * @return string
 * Function that swaps data one time initially from existing tables to new table.
 * Here if registration no is 'QA' then the " Associate ID generation loop" is activated.
 * Else after member ID check appropriate status is assigned for existing users.
 */
function qm_registration_table_swap() {

  drupal_goto('');

  //  $sub_query= db_select('users','u')
  //    ->fields('u', array('uid', 'created'));
  //  $sub_query->addJoin('LEFT', 'field_data_field_user_registration', 'reg', 'reg.entity_id = u.uid');
  //  $sub_query->addField('reg', 'field_user_registration_value');
  //  $sub_query->orderBy('uid', 'ASC');
  //
  //  $user_reg = $sub_query->execute()->fetchAll();
  //
  //  $record = '';
  //
  //  foreach ($user_reg as $user_id) {
  //
  //    if(isset($user_id->field_user_registration_value)) {
  //      // Associate Id generation loop:
  //
  //      if ($user_id->field_user_registration_value == 'QA') {
  //        $date = date('ym', $user_id->created);
  //        $id = substr($record, strpos($record, '-')+1, strpos(substr($record, strpos($record, '-')+1), '/'));
  //
  //        // As the last part /1203 remains constant for a month so if it matches the $date then we are still
  //        // in the same month. If it is greater, then we are on the first date of the next month.
  //
  //        if ($date == substr($record, -4))  {
  //          $reg_no = 'QA-' .  ($id + 1) . '/' . $date;
  //        }else{
  //          $reg_no = 'QA-1/' . $date;
  //        }
  //        $record = $reg_no;
  //
  //        $qm_user_associate = db_insert('qm_registration_user')
  //          ->fields(array(
  //                        'uid' => $user_id->uid,
  //                        'registration_no' => $record,
  //                        'status' => 'a',
  //                   ))
  //          ->execute();
  //
  //      }else{
  //        // Member registration no. check by regular expression and assigning appropriate status:
  //
  //        $ptn_master = "/^[M]{1}[-]?[0-9]+\/[0-9]+$/";
  //        $ptn_graduate = "/^[F]{1}[-]?[0-9]+\/[0-9]+$|^[0-9]+\/[0-9]+$/";
  //        $str = $user_id->field_user_registration_value;
  //
  //        preg_match($ptn_master, $str, $matches_m);
  //        preg_match($ptn_graduate, $str, $matches_g);
  //
  //        if ($matches_m[0] == $str){
  //          $status = 'p';
  //        }elseif($matches_g[0] == $str ){
  //          $status = 'g';
  //        }else{
  //          $status = 'a';
  //        }
  //
  //        $qm_user_member = db_insert('qm_registration_user')
  //          ->fields(array(
  //                        'uid' => $user_id->uid,
  //                        'registration_no' => $user_id->field_user_registration_value,
  //                        'status' => $status,
  //                   ))
  //          ->execute();
  //
  //      }
  //    }
  //
  //  }
  //  return 'Congratulations! Table swap has been successfully completed';

}




/**
 * @param $account
 * @param $view_mode
 * @param $langcode
 * @return void
 * Shows the registration no. in profile page.
 * implements hook_user_view()
 */

function qm_registration_user_view($account, $view_mode, $langcode){

  $profile_uid = $account->uid;

  $user = db_select('qm_registration_user', 'reg')
    ->fields('reg', array('uid', 'registration_no'));
  $user->condition('reg.uid', $profile_uid);

  $user_reg = $user->execute()->fetchObject();

  if (!empty($user_reg->registration_no)) {
    $reg_no = $user_reg->registration_no ;
  }else{
    $reg_no = '';
  }

  $output = '<div class="qm-reg-module-reg-no">';
  $output .= '<b>Registration No:&nbsp &nbsp</b>';
  $output .= $reg_no;
  $output .= '</div>';

  $account->content['registration_no']['reg_id']= array(
    '#type' =>'user_profile_item',
    '#title' => '',
    '#markup' => $output
  );
}



/**
 * This function deletes data from qm_registration_user table
 * when user account is canceled.
 * implements hook_user_delete()
 */
function qm_registration_user_delete($account) {
  db_delete('qm_registration_user')
    ->condition('uid', $account->uid)
    ->execute();

  db_delete('qm_registration_user_revision')
    ->condition('uid', $account->uid)
    ->execute();

  db_delete('qm_registration_user_revision_assign')
    ->condition('uid', $account->uid)
    ->execute();
}


/**
 * @param $edit
 * @param $account
 * @param $category
 * @return void
 * This function stores information in the {qm_registration_user_revision} table
 * Implements hook_user_update()
 * This hook is called when user data is updated. $edit contains the formstate of the profile edit form
 * $account contains the user account information
 * for populating the {qm_registration_user_revision} table, data is taken from $edit. but as some data are not
 * found in the $edit, so these data are taken from $account
 */

function qm_registration_user_update(&$edit, $account, $category) {

  Global $user;
  // find user status for setting different conditions
  $user_query = db_select('qm_registration_user', 'reg')
    ->fields('reg', array('uid', 'status', 'registration_no'));
  $user_query->condition('reg.uid', $account->uid);

  $user_reg_status = $user_query->execute()->fetchObject();

  $reg_num = '';
  $ref_name = '';
  $ref_relation = '';
  $ref_registration_no = '';
  $ref_contact = '';
  if(!empty($edit['user_status'])) {
    // when editing profile in admin role
    $reg_status = $edit['user_status'];
  }else{
    // when editing profile in general role
    $reg_status = $user_reg_status->status;
  }

  // now find the update value of registration number:
  if ($reg_status == 'a') {
    if(!empty($edit['associate_id'])) {
      // when editing profile in admin role
      $reg_num = $edit['associate_id'];
    }else{
      // when editing profile in general role
      $reg_num = $user_reg_status->registration_no;
    }
  }elseif ($reg_status == 'g') {
    if(!empty($edit['graduate_id'])) {
      // when editing profile in admin role
      $reg_num = $edit['graduate_id'];
    }else{
      // when editing profile in general role
      $reg_num = $user_reg_status->registration_no;
    }
  }elseif ($reg_status == 'p') {
    if(!empty($edit['promaster_id'])) {
      // when editing profile in admin role
      $reg_num = $edit['promaster_id'];
    }else{
      // when editing profile in general role
      $reg_num = $user_reg_status->registration_no;
    }
  }
  //If above condition is true then insert new row in into the comment_revision table.
  if ($reg_status == 'a') {
    if(!empty($edit['associate_ref_name'])) {
      $ref_name = $edit['associate_ref_name'];
    }
    if(!empty($edit['associate_ref_id'])) {
      $ref_registration_no = $edit['associate_ref_id'];
    }
    if(!empty($edit['associate_ref_contact'])) {
      $ref_contact = $edit['associate_ref_contact'];
    }
    if(!empty($edit['associate_ref_relation'])) {
      $ref_relation = $edit['associate_ref_relation'];
    }
  }

  $qm_user_revision = db_insert('qm_registration_user_revision')
    ->fields(array(
                  'uid' => $account->uid,
                  'account_name' => $edit['name'],
                  'euid' => $user->uid,
                  'editor_name' => $user->name,
                  'mail' => $edit['mail'],
                  'theme' => $account->theme,
                  'signature' => $edit['signature'],
                  'signature_format' => $edit['signature_format'],
                  'status' => $account->status,
                  'timezone' => $edit['timezone'],
                  'language' => $account->language,
                  'picture' => $edit['picture'],
                  'registration_no' => $reg_num,
                  'ref_name' => $ref_name,
                  'ref_relation' => $ref_relation,
                  'ref_registration_no' => $ref_registration_no,
                  'ref_contact' => $ref_contact,
                  'reg_status' => $reg_status,
                  'changed' => REQUEST_TIME,
             ))
    ->execute();


  //Find if uid exists in {qm_registration_user_revision_assign} table:
  $find_uid = db_select('qm_registration_user_revision_assign', 'ra')
    ->fields('ra', array('uid', 'revision_id'))
    ->condition('uid',$account->uid)
    ->execute()->fetchObject();

  if(!empty($find_uid->uid)) {
    $revision_update = db_update('qm_registration_user_revision_assign')
      ->fields(array(
                    'revision_id' => $qm_user_revision,
               ))
      ->condition('uid', $account->uid)
      ->execute();

  }else{

    //If uid is revised first time then insert new row into {qm_registration_user_revision_assign} table.
    $revision_new = db_insert('qm_registration_user_revision_assign')
      ->fields(array(
                    'uid' => $account->uid,
                    'revision_id' => $qm_user_revision,
               ))
      ->execute();
  }

  // now populate the field tables of the user form like blood group, first name, last name etc.
  $field_name_query = db_select('field_config_instance', 'fci')
    ->fields('fci', array('field_id','field_name','entity_type','bundle','deleted'))
    ->condition('entity_type', 'user')
    ->condition('bundle', 'user')
    ->condition('deleted', 0)
    ->execute()->fetchAll();


  foreach($field_name_query as $key) {

    $index = $key->field_name;
    $revision_table_name = 'field_revision_' . $index;

    $field_rev_insert = db_insert($revision_table_name)
      ->fields(array(
                    'entity_type' => 'user',
                    'bundle' => 'user',
                    'deleted' => 0,
                    'entity_id' => $account->uid,
                    'revision_id' => $qm_user_revision,
                    'language' => 'und',
                    'delta' => 0,
                    $index .'_value' => $edit[$index]['und'][0]['value'],
               ))
      ->execute();

    $data_table_name = 'field_data_' . $index;
    $data_table_update = db_update($data_table_name)
      ->fields(array(
                    'revision_id' => $qm_user_revision,
               ))
      ->condition('entity_id', $account->uid)
      ->execute();
  }

}
