<?php

 
/**
* Form Description
*
* return array
*/

function qm_registration_form($form, &$form_state) {

    $form['terms'] = array(
			'#title' => t('Terms & Conditions'),
			'#type' => 'textarea',
			'#rows' => 2,
			'#cols' =>30,
            '#default_value' => variable_get('terms'),
			'#description' => t('Please enter your terms.'),
		);

		$form['submit'] = array(
		    '#type' => 'submit',
		    '#value' => t('Submit')
		);

		return $form;
}


function qm_registration_form_submit($form, &$form_state){

    $result = $form_state['values']['terms'];

    variable_set('terms',$result);

}